Ingress Chrome DevTool
=====================================

-Generate on-the-fly intel reports for high AP targets  
-Track player locations  
-Track estimated player speeds (Catch GPS Cheaters!)  
-Track portal interactions  
-Works with both standard intel map and IITC (total conversion)  
-Auto scroll the COMM window to gather data!!

Install
-------

[Download Here](https://bitbucket.org/orb360/ingressparser/src/2bd78c7822dc7542ed65956397f6450ebad0299f/ingress-devtool.crx?at=master)

### Instructions

You MUST enable experimental API's for the extension to intercept map data

-Navigate to chrome://flags  
-Find "Experimental Extension APIs"  
-Click "Enable"  
-Click "Relaunch Now" (VERY IMPORTANT)  
**OR**  
-Launch Chrome with "path_to_chrome.exe --enable-experimental-extension-apis"

**Installing**

-Navigate to chrome://extensions  
-Drag ingress-devtool.crx onto the extensions page  

**Use**

-Open the intel map  
-Press F12  
-Select the "Ingress" devtool tab  
-Navigate around the map to begin loading data  

