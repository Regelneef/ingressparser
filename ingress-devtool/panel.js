/**
Angular controllers for the devtools panel
**/

//Sorting function for sorting items by level
function getValue(portal) {
	var value = portal.level / 2;
	for(var id in portal.mods) {
		var mod = portal.mods[id];
		if(typeof mod.type == 'undefined' || typeof mod.rarity == 'undefined')
			continue;
		switch(mod.type + mod.rarity) {
			case "MC":
			case "HC":
				value = value + 1;break;
			case "MR":
			case "HR":
				value = value + 2;break;
			case "MV":
			case "HV":
				value = value + 3;break;
			case "SC":	
				value = value - 0.25;break;
			case "SR":
				value = value - 0.5;break;
			case "SV":
			case "AR":
			case "TR":
				value = value - 1;break;
			case "LR":
				value = value + 0.25;break;
			default:
				console.log("Unknown type+rarity: " + mod.type + mod.rarity);
		}
	}
	
	value = value + portal.links;
	value = value + (portal.fieldsNum * 2);
	value = value + (portal.captured / 5);
	
	return value;
}

sortByCustom = function(b,a) {
	return getValue(a)-getValue(b);
}
sortByLevel = function(b,a) {
return a.level-b.level;
}
sortByLevelName = function(b,a) {
	var sort = a.level-b.level;
	if(sort == 0) {
		if(typeof a.name == 'undefined' && typeof b.name == 'undefined')
			return 0;
		else if(typeof a.name == 'undefined')
			return 1;
		else if(typeof b.name == 'undefined')
			return -1;
		else
			sort = a.name.localeCompare(b.name)*-1;
	}
	
	return sort;
}
sortByCaptured = function(b,a) {
return a.captured-b.captured;
}

//Sorting function for sorting items by ap value
sortByAP = function(b,a) {
return a.ap-b.ap;
}
//Sorting function for sorting items by ap value
sortByName = function(b,a) {
return a.name.localeCompare(b.name);
}
//Sorting function for sorting items by ap value
sortByEpoch = function(b,a) {
return a.epoch-b.epoch;
}
sortByEpochR = function(b,a) {
return a.epoch-b.epoch;
}
sortByTotal = function(b,a) {
return a.total-b.total;
}
sortByCaptures = function(b,a) {
return a.captures-b.captures;
}
sortByFields = function(b,a) {
return a.fields-b.fields;
}
sortByUnfields = function(b,a) {
return a.unfields-b.unfields;
}
sortByLinks = function(b,a) {
return a.links-b.links;
}
sortByUnlinks = function(b,a) {
return a.unlinks-b.unlinks;
}
sortByResoDeploy = function(b,a) {
return a.resonators-b.resonators;
}
sortByResoDestroy = function(b,a) {
return a.unresonators-b.unresonators;
}

var portalSorter = sortByAP;

function APCtrl($scope, $rootScope, $location) {
    console.log("PortalListCtrl");
    
    $scope.gotolatlng = function(lat,lng) {
        chrome.extension.sendMessage({"tabid":chrome.devtools.inspectedWindow.tabId, "lat":lat,"lng":lng}, function(response) {
            console.log("msg response");
        });
    }
	
    $scope.loadPortal = function(id) {
        $location.path( "/portal/" + id );
    }
    
    $scope.sortByAP = function(id) {
        portalSorter = sortByAP;
        refreshAPCtrl($scope);
    }
    
    $scope.sortByLevel = function(id) {
        portalSorter = sortByLevel;
        refreshAPCtrl($scope);
    }
	
	$scope.sortByCustom = function(id) {
        portalSorter = sortByCustom;
        refreshAPCtrl($scope);
    }
    
    $scope.sortByCaptured = function(id) {
        portalSorter = sortByCaptured;
        refreshAPCtrl($scope);
    }
    
    $scope.$on('parse', function() {
        refreshAPCtrl($scope);
		$scope.$apply();
    });
	
	$scope.$on('refresh', function() {
        refreshAPCtrl($scope);
    });
    
    refreshAPCtrl($scope);
}

function refreshAPCtrl($scope) {
	//console.log("^^^^^^^^^^^^^^^^^^^^^^^^BEGIN^^^^^^^^^^^^^^^^^^^^^^");
    if(typeof $scope.aliens == 'undefined')
        $scope.aliens = [];
    if(typeof $scope.resistance == 'undefined')
        $scope.resistance = [];
    if(typeof $scope.unknown == 'undefined')
        $scope.unknown = [];
    
    var rid = 0;
    var aid = 0;
	var uid = 0;
    
    for(id in portals) {
        var portal = portals[id];
        if(portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6) {
			var hourOld = Date.now() - 3600000;
			if(portal.lastSeen < hourOld)
				portal.stale = true;
			else
				portal.stale = false;
            if(portal.team == 'RESISTANCE') {
            //console.log("ID" + portal.id + " " + portal.name);
                if(typeof $scope.resistance[rid] != 'undefined') {
                    //console.log("  updating R" + rid + portal.name);
					refreshPortalInList($scope.resistance[rid], portal);
                } else {
                    $scope.resistance[rid] = new Object();
					refreshPortalInList($scope.resistance[rid], portal);
                }
                rid = rid + 1;
            } else if (portal.team == 'ENLIGHTENED') {
                if(typeof $scope.aliens[aid] != 'undefined') {
                    //console.log("  updating A" + aid + portal.name);
					refreshPortalInList($scope.aliens[aid], portal);
                } else {
					$scope.aliens[aid] = new Object();
					refreshPortalInList($scope.aliens[aid], portal);
                }
                aid = aid + 1;
            } else { //Unclaimed
                if(typeof $scope.unknown[uid] != 'undefined') {
					refreshPortalInList($scope.unknown[uid], portal);
                } else {
					$scope.unknown[uid] = new Object();
					refreshPortalInList($scope.unknown[uid], portal);
                }
                uid = uid + 1;
            } 
        }
    }
    
    $scope.resistance.length = rid;
    $scope.aliens.length = aid;
    $scope.unknown.length = uid;
    
    $scope.resistance.sort(portalSorter);
    $scope.aliens.sort(portalSorter);
    
    $scope.rcount = $scope.resistance.length;
    $scope.acount = $scope.aliens.length;
    $scope.ucount = $scope.unknown.length;
	//console.log("---------------------END-----------------------");
}

function refreshPortalInList(list, portal) {
                    list.id = portal.id;
                    list.team = portal.team;
                    list.name = portal.name.substring(0,20);
                    list.lat = portal.lat;
                    list.lng = portal.lng;
                    list.level = portal.level
                    list.links = portal.links;
                    list.fields = portal.fields;
                    list.fieldsNum = portal.fieldsNum;
                    list.ap = portal.ap;
                    list.resoCount = portal.resoCount;
                    list.resonators = portal.resonators;
                    list.mods = portal.mods;
                    list.captured = portal.captured;
					var seen = ((Date.now() - portal.lastSeen) / 3600000);
                    list.lastSeen = seen.toFixed(2);
					list.stale = portal.stale;
					var value = getValue(portal);
					list.score = value.toFixed(2);
}

var showAllPlayers = false;

function PlayerListCtrl($scope, $rootScope, $location) {
    console.log("PlayerListCtrl");
    
	
    $scope.$on('parse', function() {
        refreshPlayerListCtrl($scope);
        $scope.$apply();
    });
    
    $scope.$on('refresh', function() {
        refreshPlayerListCtrl($scope);
    });
    
    $scope.loadPlayer = function(id) {
        $location.path( "/player/" + id );
    }
    
    $scope.showAll = function(id) {
        if(showAllPlayers)
			showAllPlayers = false;
		else
			showAllPlayers = true;
    }
    
    refreshPlayerListCtrl($scope);
    
}

function refreshPlayerListCtrl($scope) {
    if(typeof $scope.aliens == 'undefined')
        $scope.aliens = [];
    if(typeof $scope.resistance == 'undefined')
        $scope.resistance = [];
    if(typeof $scope.unknown == 'undefined')
        $scope.unknown = [];
    
    var rid = 0;
    var aid = 0;
    var uid = 0;
	
	var stats = [];
	for(var i = 0; i < 9; i++) {
		var stat = new Object();
		stat.r = 0;
		stat.e = 0;
		stat.count = i;
		stats[i] = stat;
	}
    
    for(id in players) {
        players[id].show = false;
    }
    
    for(id in plexts) {
        //console.log("plext");
        var plext = plexts[id];
        for(var i = 0; i < plext.portalids.length; i++) {
        //console.log("portal");
            var portal = portals[plext.portalids[i]];
            if(typeof portal != 'undefined' && portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6) {
                players[plext.playerid].show = true;
                
                //console.log("Showing1:" + players[plext.playerid].name);
            }
        }
    }
    
    for(id in portals) {
        var portal = portals[id];
        if(portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6) { 
            for(var i = 0; i < 8; i++) {
                if(typeof portal.resonators[i] != 'undefined' &&
				typeof players[portal.resonators[i].playerid] != 'undefined')
					players[portal.resonators[i].playerid].show=true;
            }
        }
    }
    
    for(id in players) {
        var player = players[id];
            //console.log("playershow: " + player.show);
            if(!player.show && !showAllPlayers)
                continue;
            if(player.team == 'RESISTANCE') {
				stats[player.level].r++;
                if(typeof $scope.resistance[rid] != 'undefined') {
                    $scope.resistance[rid].id = player.id;
                    $scope.resistance[rid].team = player.team;
                    $scope.resistance[rid].name = player.name;
                    $scope.resistance[rid].names = player.names.length > 1;
                    $scope.resistance[rid].level = player.level;
                    $scope.resistance[rid].lastSeen = player.lastSeen;
                } else {
                    $scope.resistance[rid] = new Object();
                    $scope.resistance[rid].id = player.id;
                    $scope.resistance[rid].team = player.team;
                    $scope.resistance[rid].name = player.name;
                    $scope.resistance[rid].names = player.names.length > 1;
                    $scope.resistance[rid].level = player.level;
                    $scope.resistance[rid].lastSeen = player.lastSeen;
                }
                rid = rid + 1;
            } else if (player.team == 'ENLIGHTENED') {
				stats[player.level].e++;
                if(typeof $scope.aliens[aid] != 'undefined') {
                    $scope.aliens[aid].id = player.id;
                    $scope.aliens[aid].team = player.team;
                    $scope.aliens[aid].name = player.name;
                    $scope.aliens[aid].names = player.names.length > 1;
                    $scope.aliens[aid].level = player.level;
                    $scope.aliens[aid].lastSeen = player.lastSeen;
                } else {
					$scope.aliens[aid] = new Object();
                    $scope.aliens[aid].id = player.id;
                    $scope.aliens[aid].team = player.team;
                    $scope.aliens[aid].name = player.name;
                    $scope.aliens[aid].names = player.names.length > 1;
                    $scope.aliens[aid].level = player.level;
                    $scope.aliens[aid].lastSeen = player.lastSeen;
                }
                aid = aid + 1;
            } else {
                if(typeof $scope.unknown[uid] != 'undefined') {
                    $scope.unknown[uid].id = player.id;
                    $scope.unknown[uid].team = player.team;
                    $scope.unknown[uid].name = player.name;
                    $scope.unknown[uid].level = player.level;
                    $scope.unknown[uid].lastSeen = player.lastSeen;
                } else {
					$scope.unknown[uid] = new Object();
                    $scope.unknown[uid].id = player.id;
                    $scope.unknown[uid].team = player.team;
                    $scope.unknown[uid].name = player.name;
                    $scope.unknown[uid].level = player.level;
                    $scope.unknown[uid].lastSeen = player.lastSeen;
                }
                uid = uid + 1;
            }
    }
    
    $scope.resistance.length = rid;
    $scope.aliens.length = aid;
    $scope.unknown.length = uid;
    
    $scope.resistance.sort(sortByLevelName);
    $scope.aliens.sort(sortByLevelName);
    
    $scope.rcount = $scope.resistance.length;
    $scope.acount = $scope.aliens.length;
    $scope.ucount = $scope.unknown.length;
	
	$scope.stats = stats;
}

function PlayerCtrl($scope, $rootScope, $routeParams, $location) {
    console.log("PlayerCtrl:" + $routeParams.id);
    
    $scope.player = players[$routeParams.id];
    
    $scope.loadPortal = function(id) {
        $location.path( "/portal/" + id );
    }
    
    $scope.showFind = function(id) {
        if(typeof $scope.player.portalid != undefined)
            return true;
        return false;
    }
    
    $scope.plexts = [];
    var pid = 0;
    
    for(var id in plexts) {
        if(plexts[id].playerid === $scope.player.id) {
            $scope.plexts[pid] = plexts[id];
            var date = new Date(plexts[id].epoch);
            var string = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
            
            
            $scope.plexts[pid].date = string;
            if(typeof portals[plexts[id].portalids[0]] != 'undefined') {
                $scope.plexts[pid].portalname1 = portals[plexts[id].portalids[0]].name;
                $scope.plexts[pid].portalid1 = portals[plexts[id].portalids[0]].id;
            }
            if(typeof portals[plexts[id].portalids[1]] != 'undefined') {
                $scope.plexts[pid].portalname2 = portals[plexts[id].portalids[1]].name;
                $scope.plexts[pid].portalid2 = portals[plexts[id].portalids[1]].id;
            }
                
            pid = pid + 1;
        }
    }
    
    $scope.plexts.sort(sortByEpoch);
    
    var prevPort1 = null;
    var prevPort2 = null;
    var prevTime = null;
    
    //console.log("Begin");
    
	var hash = new Object();
	
    for(var i = ($scope.plexts.length-1); i >= 0 ; i--) {
        var plext = $scope.plexts[i];
            
            var portal1 = null;
            var portal2 = null;
            
            if(plext.portalids.length >= 1)
                portal1 = portals[plext.portalids[0]];
            if(plext.portalids.length >= 2)
                portal2 = portals[plext.portalids[1]];
                    
            if(prevTime != null) {
                plext.hours = ((plext.epoch - prevTime) / 1000 / 60 / 60);
                plext.hours = Math.round(plext.hours*100)/100;
                
                    
                plext.distance = distanceBetween(portal1,portal2,prevPort1,prevPort2);
                
                if(plext.hours > 0) {
                    plext.speed = plext.distance / plext.hours;
                    plext.speed = Math.round(plext.speed*100)/100;
                    if(plext.speed < 6) {
                        plext.speedstyle = "WALKING";
                    } else if(plext.speed < 26) {
                        plext.speedstyle = "RUNNING";
                    } else if(plext.speed < 130) {
                        plext.speedstyle = "BIKING";
                    } else if(plext.speed < 200) {
                        plext.speedstyle = "CAR";
                    } else {
                        plext.speedstyle = "CHEAT";
                    }
                } else {
                    plext.speedstyle = "NODATA";
                    plext.speed = 0;
                }
            }
            
            prevPort1 = portal1;
            prevPort2 = portal2;
            prevTime = plext.epoch;
		
		//Count for heatmap
        if(plext.portalids.length <= 0)
			continue;
		
		var portal = portals[plext.portalids[0]];
		
		if(typeof hash[portal.id] == 'undefined') {
			hash[portal.id] = 0;
		}
		
		hash[portal.id]++;
    }
	
	//console.log("hash len" + Object.size(hash));
    var testData = new Object();
    testData.data = [];
    testData.max = 0;
	
	//Convert hash into heatmap readable format
    for(id in hash) {
		var portal = portals[id];
		
		if(hash[id] > testData.max)
			testData.max = hash[id];
			
        var pair = new Object();
        pair.lat = portal.lat/1000000;
        pair.lon = portal.lng/1000000;
        pair.value = hash[id];
        testData.data[testData.data.length] = pair;
    }
    //console.log("test size" + testData.data.length);
	//Setup and add heatmap to DOM
    var baseLayer = L.tileLayer(
                    'http://{s}.tile.cloudmade.com/4410b1d37d0143b38df9302bddd0e5e0/65053/256/{z}/{x}/{y}.png',{
                        attribution: 'Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://cloudmade.com">CloudMade</a>',
                        maxZoom: 18
                    }
                );
    var heatmapLayer = L.TileLayer.heatMap({
                    radius: 20,
                    opacity: 0.8,
                    gradient: {
                        0.45: "rgb(0,0,255)",
                        0.55: "rgb(0,255,255)",
                        0.65: "rgb(0,255,0)",
                        0.95: "yellow",
                        1.0: "rgb(255,0,0)"
                    }
                });
    heatmapLayer.addData(testData.data);
    var overlayMaps = {
        'Heatmap': heatmapLayer
    };
 
    var controls = L.control.layers(null, overlayMaps, {collapsed: false});
 
	if(typeof testData.data[0] != 'undefined')
    var map = new L.Map('heatmapArea', {
        center: new L.LatLng(testData.data[0].lat, testData.data[0].lon),
        zoom: 10,
        layers: [baseLayer, heatmapLayer]
    });
}

function distanceBetween(plex1port1, plex1port2, plex2port1, plex2port2) {
    var ret = 500000;
    if(plex1port1 != null) {
        if(plex2port1 != null) {
            var d = distance(
                        plex1port1.lat/1E6,
                        plex1port1.lng/1E6,
                        plex2port1.lat/1E6,
                        plex2port1.lng/1E6);
            //console.log(plex1port1.name + "-" + plex2port1.name + "=" + d);
            if(d < ret)
                ret = d;
        }
        if( plex2port2 != null) {
            var d = distance(
                        plex1port1.lat/1E6,
                        plex1port1.lng/1E6,
                        plex2port2.lat/1E6,
                        plex2port2.lng/1E6);
            //console.log(plex1port1.name + "-" + plex2port2.name + "=" + d);
            if(d < ret)
                ret = d;
        }
    }
    if(plex1port2 != null) {
        if(plex2port1 != null) {
            var d = distance(
                        plex1port2.lat/1E6,
                        plex1port2.lng/1E6,
                        plex2port1.lat/1E6,
                        plex2port1.lng/1E6);
            //console.log(plex1port2.name + "-" + plex2port1.name + "=" + d);
            if(d < ret)
                ret = d;
        }
        if(plex2port2 != null) {
            var d = distance(
                        plex1port2.lat/1E6,
                        plex1port2.lng/1E6,
                        plex2port2.lat/1E6,
                        plex2port2.lng/1E6);
            //console.log(plex1port2.name + "-" + plex2port2.name + "=" + d);
            if(d < ret)
                ret = d;
        }
    }
    
    if(ret == 500000)
        return 0;
    return ret;
}

function distance(t1,t2,n1,n2, precision) {
   // console.log(t1 + " " + t2 + " " + n1 + " " + n2);
  // default 4 sig figs reflects typical 0.3% accuracy of spherical model
  if (typeof precision == 'undefined') precision = 4;
  
  var lat1 = deg2rad(t1);
  var lon1 = deg2rad(t2);
  var lat2 = deg2rad(n1);
  var lon2 = deg2rad(n2);
  
  var R = 6371;
  var dLat = lat2 - lat1;
  var dLon = lon2 - lon1;
  
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(lat1) * Math.cos(lat2) * 
          Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return d.toPrecision(precision);
}

function deg2rad(deg) {
		rad = deg * Math.PI/180; // radians = degrees * pi/180
		return rad;
	}

function PortalCtrl($scope, $rootScope, $routeParams, $location) {
    console.log("PortalCtrl:" + $routeParams.id);
    $scope.portal = portals[$routeParams.id];
    
    $scope.gotolatlng = function(lat,lng) {
        console.log("in function");
        chrome.extension.sendMessage({"tabid":chrome.devtools.inspectedWindow.tabId, "lat":lat,"lng":lng}, function(response) {
            console.log("msg response");
        });
    }
    
    $scope.loadPlayer = function(id) {
        $location.path( "/player/" + id );
    }
    
    $scope.plexts = [];
    var pid = 0;
   
    for(var id in plexts) {
        if(plexts[id].portalids.length <= 0)
            continue;
        if((plexts[id].portalids.length >= 1 && plexts[id].portalids[0] === $scope.portal.id) ||
         (plexts[id].portalids.length >= 2 && plexts[id].portalids[1] === $scope.portal.id)) {
            $scope.plexts[pid] = plexts[id];
            var date = new Date(plexts[id].epoch);
            var string = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
            
            
            $scope.plexts[pid].date = string;
            if(typeof players[plexts[id].playerid] != 'undefined') {
                $scope.plexts[pid].playername = players[plexts[id].playerid].name;
                $scope.plexts[pid].playerteam = players[plexts[id].playerid].team;
            }
			
            pid = pid + 1;
        }
    }
    
    $scope.plexts.sort(sortByEpoch);
	
	var lasttime = 0;
	var lastteam = 'NUETRAL';
	var resistown = 0;
	var alienown = 0;
	
	for(var i = ($scope.plexts.length - 1); i >= 0; i--) {
		var plext = $scope.plexts[i];
		if(plext.action == 'CAPTURED' && plext.type == 'PORTAL') {
			if(lasttime != 0) {
				var time = plext.epoch - lasttime;
				if(time < 0)
					alert('less than 0');
				var team = lastteam;
				if(team == 'RESISTANCE')
					resistown += time;
				else
					alienown += time;
			}
			
			lasttime = plext.epoch;
			lastteam = players[plext.playerid].team;
		}
		if(i == 0 && lasttime != 0) {
			var time = Date.now() - lasttime;
			var team = lastteam;
			if(team == 'RESISTANCE')
				resistown += time;
			else
				alienown += time;
		}
	}
	
	$scope.alienowner = Math.round((alienown / 1000 / 60 / 60 / 24)*100)/100;
	$scope.resistowner = Math.round((resistown / 1000 / 60 / 60 / 24)*100)/100;
	$scope.alienpct =  Math.round(alienown/(alienown + resistown)*100);
	$scope.resistpct =  Math.round(resistown/(alienown + resistown)*100);
}

var scan = [];
var doScan = function() {
	//Remove any portals that were updated alongside last time.
	var hourOld = Date.now() - 3600000;
	for(var i = 0; i < scan.length; i++) {
		if(scan[i].lastSeen > hourOld) {
			scan.splice(i,1);
		}
	}

	if(scan.length <= 0)
		return;
		
	var portal = scan[0];
    scan.splice(0,1);
	
	chrome.extension.sendMessage({"tabid":chrome.devtools.inspectedWindow.tabId, "lat":portal.lat,"lng":portal.lng}, function(response) {
        console.log("msg response");
    });
		
	setTimeout(doScan,15000);
}

function HeaderCtrl($scope, $location, $rootScope) {
    console.log("HeaderCtrl");
    $scope.intercepts = 0;
    
    refreshHeaderCtrl($scope)
    
    $scope.$on('parse', function() {
        refreshHeaderCtrl($scope);
        $scope.$apply();
    });
	
	$scope.$on('queue', function() {
		$scope.queue = queue.length;
        //$scope.$apply();
    });

    //We can't navigate the angular app via links in an extension.
    //So we have to call $location.path to set the ng-view path
    $scope.aplink = function() {
        $location.path( "/" );
    }
    
    $scope.summarylink = function() {
        $location.path( "/summary" );
    }
    
    $scope.playerlink = function() {
        $location.path( "/players" );
    }
    
    $scope.portallink = function() {
        $location.path( "/portals" );
    }
    
    $scope.heatmaplink = function() {
        $location.path( "/heatmap" );
    }
    
    $scope.chatloglink = function() {
        $location.path( "/chatlog" );
    }
    
    $scope.refreshlink = function() {
        $rootScope.$broadcast('refresh');
    }
    
    //Reset model objects and clear indexeddb tables
    $scope.clearlink = function() {
        ingressdb.indexedDB.clean();
    }
	
	$scope.scan = function() {
		scan = [];
		var hourOld = Date.now() - 172800000;
		for(id in portals) {
			var portal = portals[id];
			if(portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6)
			if(portals[id].lastSeen < hourOld)
				scan[scan.length]=portal;
		}
		doScan();
    }
	
	$scope.start = function() {
		chrome.extension.sendMessage({"scroll":1}, function(response) {
            console.log("msg response");
        });
    }
    
    $scope.stop = function() {
		chrome.extension.sendMessage({"scroll":0}, function(response) {
            console.log("msg response");
        });
    }
}

function refreshHeaderCtrl($scope) {
        $scope.intercepts = $scope.intercepts + 1;
        $scope.players = Object.size(players);
        $scope.portals = Object.size(portals);
        $scope.plexts = Object.size(plexts);
        $scope.chats = Object.size(chats);
        $scope.queue = queue.length;
		$scope.scanLength = scan.length + " " + (scan.length * 15 / 60) + "m";
		
        $scope.maxLatE6 = maxLatE6;
        $scope.maxLngE6 = maxLngE6;
        $scope.minLatE6 = minLatE6;
        $scope.minLngE6 = minLngE6;
        $scope.now = Date.now();
		$scope.stale = 0;
		var hourOld = Date.now() - 172800000;
		for(id in portals) {
			var portal = portals[id];
			if(portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6)
			if(portals[id].lastSeen < hourOld)
				$scope.stale++;
		}
}

//queue content for processing
queueContent = function(content, encoding) {
//console.log("queue");
    queue[queue.length] = content;
	injector.invoke(['MyService', function(MyService){
            MyService.queue();
        }]);
}

var maxLatE6 = 0;
var maxLngE6 = 0;
var minLatE6 = 0;
var minLngE6 = 0;

//Get content from resource and queue filter by json
ResourceQueuer = function(resource) 
{
//console.log("intercept");
//console.log(resource);
    for (var key in resource.request.headers) {
        var header = resource.request.headers[key];
        if(header.name.toLowerCase() == 'content-type' && header.value.indexOf("application/json") !== -1) {
            var obj = JSON.parse(resource.request.postData.text);
            if(typeof obj.maxLatE6 != 'undefined') {
                maxLatE6 = obj.maxLatE6;
                maxLngE6 = obj.maxLngE6;
                minLatE6 = obj.minLatE6;
                minLngE6 = obj.minLngE6;
            }
            resource.getContent(queueContent);  
        }
    }
}

//Intercept all HTTP responses for processing
chrome.experimental.devtools.network.onRequestFinished.addListener(ResourceQueuer);

//Process Queue
var queue = new Array();

function SummaryCtrl($scope, $rootScope, $location) {
    console.log("SummaryCtrl");
    
    $scope.$on('parse', refreshSummaryCtrl($scope));
    
    $scope.loadPlayer = function(id) {console.log("Loading player:" + id);
        $location.path( "/player/" + id );
    }
    
    refreshSummaryCtrl($scope);
}

function refreshSummaryCtrl($scope) {
	var oneday = Date.now() - 86400000;
	var oneweek = Date.now() - 604800000;
	
    var cap = 10;
    var stats = [];
	
	for(id in portals) {
		var portal = portals[id];
		if(portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6) {
			stats[stats.length] = portal;
			portal.total = 0;
			portal.captures = 0;
			
			for(var j = 0; j < portal.plexts.length; j++) {
				var plext = plexts[portal.plexts[j]];
				
				if(typeof plext == 'undefined' || plext.epoch < oneweek)
					continue;
					
				portal.total = portal.total + 1;
				switch(plext.type + plext.action) {
					case "PORTALCAPTURED":portal.captures = portal.captures + 1;break;
					default: //console.log("unknown stat:"+portal.name+":" +plext.type + plext.action);
				}
			}
		}
	}
	
	stats.sort(sortByTotal);
    $scope.topActivePortals = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].total == 0)
			continue;
        $scope.topActivePortals[$scope.topActivePortals.length] = stats[i];
    }
	
	stats.sort(sortByCaptures);
    $scope.topContested = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].total == 0)
			continue;
        $scope.topContested[$scope.topContested.length] = stats[i];
    }
	
	stats = [];
	
    for(id in players) {
        stats[stats.length] = players[id];
        players[id].total = 0;
        players[id].captures = 0;
        players[id].fields = 0;
        players[id].unfields = 0;
        players[id].links = 0;
        players[id].unlinks = 0;
        players[id].resonators = 0;
        players[id].unresonators = 0;
        for(var j = 0; j < players[id].plexts.length; j++) {
            var plext = plexts[players[id].plexts[j]];
			if(typeof plext == 'undefined')
				continue;
			//Skip if it's older than a week
			if(plext.epoch < oneweek)
				continue;
			//Skip if it's outside window
            var skip = true;
            for(var k = 0; k < plext.portalids.length; k++) {
                var portal = portals[plext.portalids[k]];
				
                if(typeof portal !== 'undefined' && portal.lat > minLatE6 && portal.lat < maxLatE6 && portal.lng > minLngE6 && portal.lng < maxLngE6)
                    skip = false;
            }
            if(skip)
                continue;
			//Increase player total
            players[id].total = players[id].total + 1;
            switch(plext.type + plext.action) {
                case "PORTALCAPTURED":players[id].captures = players[id].captures + 1;break;
                case "CONTROL_FIELDCREATED":players[id].fields = players[id].fields + 1;break;
                case "CONTROL_FIELDDESTROYED":players[id].unfields = players[id].unfields + 1;break;
                case "LINKCREATED":players[id].links = players[id].links + 1;break;
                case "LINKDESTROYED":players[id].unlinks = players[id].unlinks + 1;break;
                case "RESONATORDESTROYED":players[id].unresonators = players[id].unresonators + 1;break;
                case "RESONATORDEPLOYED":players[id].resonators = players[id].resonators + 1;break;
                default: //console.log("unknown stat:"+players[id].name+":" +plext.type + plext.action);
            }
        }
    }
    
    
    if(stats.length < 10)
        cap = stats.length;
        
    stats.sort(sortByTotal);
    $scope.topActive = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].total == 0)
			continue;
        $scope.topActive[$scope.topActive.length] = stats[i];
    }
    stats.sort(sortByCaptures);
    $scope.topCappers = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].captures == 0)
			continue;
        $scope.topCappers[$scope.topCappers.length] = stats[i];
    }
    stats.sort(sortByFields);
    $scope.topFielders = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].fields == 0)
			continue;
        $scope.topFielders[$scope.topFielders.length] = stats[i];
    }
    stats.sort(sortByUnfields);
    $scope.topUnfielders = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].unfields == 0)
			continue;
        $scope.topUnfielders[$scope.topUnfielders.length] = stats[i];
    }
    stats.sort(sortByLinks);
    $scope.topLinkers = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].links == 0)
			continue;
        $scope.topLinkers[$scope.topLinkers.length] = stats[i];
    }
    stats.sort(sortByUnlinks);
    $scope.topUnlinkers = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].unlinks == 0)
			continue;
        $scope.topUnlinkers[$scope.topUnlinkers.length] = stats[i];
    }
    stats.sort(sortByResoDeploy);
    $scope.topDeployers = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].resonators == 0)
			continue;
        $scope.topDeployers[$scope.topDeployers.length] = stats[i];
    }
    stats.sort(sortByResoDestroy);
    $scope.topDestroyers = [];
    for(var i = 0; i < cap; i++) {
		if(stats[i].unresonators == 0)
			continue;
        $scope.topDestroyers[$scope.topDestroyers.length] = stats[i];
    }
}

function ChatLogCtrl($scope, $rootScope, $location) {
    console.log("ChatLogCtrl");
	
	var chatlist = [];
	
	for(id in chats) {
		chatlist[chatlist.length] = chats[id];
		var date = new Date(chats[id].epoch);
        var string = date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
        
        chatlist[chatlist.length-1].date = string;
	}
	
	chatlist.sort(sortByEpochR);
	$scope.chats = chatlist;
}

function HeatmapCtrl($scope, $rootScope, $location) {
    console.log("HeatmapCtrl");
	
	//Count up values and populate hash
	var hash = new Object();
    for(id in plexts) {
		//console.log("part 1");
		var plext = plexts[id];
        if(plext.portalids.length <= 0)
            continue;
		
		//console.log("part 2");
		var portal = portals[plext.portalids[0]];
		
		if(typeof hash[portal.id] == 'undefined') {
			hash[portal.id] = 0;
			
			//console.log("part 3");
		}
		
		hash[portal.id]++;
	}
	//console.log("hash len" + Object.size(hash));
    var testData = new Object();
    testData.data = [];
    testData.max = 0;
	
	//Convert hash into heatmap readable format
    for(id in hash) {
		var portal = portals[id];
		
		if(hash[id] > testData.max)
			testData.max = hash[id];
			
        var pair = new Object();
        pair.lat = portal.lat/1000000;
        pair.lon = portal.lng/1000000;
        pair.value = hash[id];
        testData.data[testData.data.length] = pair;
    }
    //console.log("test size" + testData.data.length);
	//Setup and add heatmap to DOM
    var baseLayer = L.tileLayer(
                    'http://{s}.tile.cloudmade.com/4410b1d37d0143b38df9302bddd0e5e0/997/256/{z}/{x}/{y}.png',{
                        attribution: 'Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://cloudmade.com">CloudMade</a>',
                        maxZoom: 18
                    }
                );
    var heatmapLayer = L.TileLayer.heatMap({
                    radius: 20,
                    opacity: 0.8,
                    gradient: {
                        0.45: "rgb(0,0,255)",
                        0.55: "rgb(0,255,255)",
                        0.65: "rgb(0,255,0)",
                        0.95: "yellow",
                        1.0: "rgb(255,0,0)"
                    }
                });
    heatmapLayer.addData(testData.data);
    var overlayMaps = {
        'Heatmap': heatmapLayer
    };
 
    var controls = L.control.layers(null, overlayMaps, {collapsed: false});
 
    var map = new L.Map('heatmapArea', {
        center: new L.LatLng(testData.data[0].lat, testData.data[0].lon),
        zoom: 6,
        layers: [baseLayer, heatmapLayer]
    });
 
    //controls.addTo(map);
	//L.control.pan().addTo(map);
	//L.control.zoom().addTo(map);
}

//Angular module setup and pathing
var myModule = angular.module('ingress-app', []).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: chrome.extension.getURL('views/ap.html'),
        controller: APCtrl
      }).
      when('/summary', {
        templateUrl: chrome.extension.getURL('views/summary.html'),
        controller: SummaryCtrl
      }).
      when('/players', {
        templateUrl: chrome.extension.getURL('views/players.html'),
        controller: PlayerListCtrl
      }).
	  when('/player/:id', {
        templateUrl: chrome.extension.getURL('views/player.html'),
        controller: PlayerCtrl
      }).
	  when('/portal/:id', {
        templateUrl: chrome.extension.getURL('views/portal.html'),
        controller: PortalCtrl
      }).
	  when('/heatmap', {
        templateUrl: chrome.extension.getURL('views/heatmap.html'),
        controller: HeatmapCtrl
      }).
	  when('/chatlog', {
        templateUrl: chrome.extension.getURL('views/chatlog.html'),
        controller: ChatLogCtrl
      }).
	  otherwise({
        redirectTo: '/'
      });
  }]).factory('MyService', function($rootScope) {//Service for sending gotone events to refresh controllers
        return {
            parse: function() {
                //console.log('broadcasting');
				if(queue.length == 0)
					$rootScope.$broadcast('parse');
            },
			queue: function() {
                //console.log('broadcasting');
                $rootScope.$broadcast('queue');
            }  			
        }
    });

//This injector will allow non-angular code to access the service and send broadcasts
var injector;
angular.element(document).ready(function() {
  injector = angular.bootstrap(document, ['ingress-app']);
});

//Function to process queue entries
process = function() {
    //Exit if queue is empty
    if(queue.length == 0)
        return;
    
    //Pop off the first JSON text object
    var content = queue[0];
    queue.splice(0,1);
    
    //Parse it
    var object = null;
    try {
        object = JSON.parse(content);
    } catch(err) {
       console.log("Parse error:" + content);
       return;
    }
    
    //Parse the intel JSON and put it in the globals.
    parse(object);
    delete object;
	delete content;
    //console.log("portals: " + Object.size(portals));
    //console.log("players: " + Object.size(players));
    //console.log("plexts: " + Object.size(plexts));
    //If we're done with the queue, refresh the controllers
    //if(queue.length == 0)
    injector.invoke(['MyService', function(MyService){
            MyService.parse();
        }]);
}

//Run the processor every 300 ms
setInterval(process,300);
  