"select portals.id, portals.team, portals.name, portals.links, portals.fields, portals.captured, portals.updated " +
				"from portals " +
				"where (latitude between ? and ?) " +
				"and (longitude between ? and ?) "

select 
portals.id, portals.team, portals.name, portals.links, portals.fields,
portals.captured, portals.updated,portal.latitude,portal.longitude,
rzero.level,rone.level,rtwo.level,rthree.level,rfour.level,rfive.level,rsix.level,rseven.level,
mzero.type,mzero.rarity,mone.type,mone.rarity,mtwo.type,mtwo.rarity,mthree.type,mthree.rarity
from portals
left outer join  resonators rzero  on rzero.portalid=portals.id  and rzero.slot=0
left outer join  resonators rone   on rone.portalid=portals.id   and rone.slot=1
left outer join  resonators rtwo   on rtwo.portalid=portals.id   and rtwo.slot=2
left outer join  resonators rthree on rthree.portalid=portals.id and rthree.slot=3
left outer join  resonators rfour  on rfour.portalid=portals.id  and rfour.slot=4
left outer join  resonators rfive  on rfive.portalid=portals.id  and rfive.slot=5
left outer join  resonators rsix   on rsix.portalid=portals.id   and rsix.slot=6
left outer join  resonators rseven on rseven.portalid=portals.id and rseven.slot=7
left outer join  mods mzero  on mzero.portalid=portals.id  and mzero.slot=0
left outer join  mods mone   on mone.portalid=portals.id   and mone.slot=1
left outer join  mods mtwo   on mtwo.portalid=portals.id   and mtwo.slot=2
left outer join  mods mthree on mthree.portalid=portals.id and mthree.slot=3
where (latitude between ? and ?)
and (longitude between ? and ?)