console.log("Content Script Loaded");

//CS -> BG
function notifyBackground(msg) {
	chrome.extension.sendMessage(msg, function(response) {
    });
}

function log(text) {
	console.log(text);
	var msg = new Object();
	msg.msg = "log";
	msg.log = text;
	notifyBackground(msg);
}

var timer = 0;
var count = 0;

//BG -> CS
chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
	log("CS: Recieved Scan Message");
	count = 0;
    
	document.getElementById("pl_tab_all").click();
	
	timer = setInterval(function() {
		count = count + 1;
		log("CS: Scan " + count);
		if(count > 3) {
			log("CS: Done Scanning.");
            clearInterval(timer);
			notifyBackground({msg:"done"});
		}
		document.getElementById("plext_container").scrollTop = 150;
		document.getElementById("plext_container").scrollTop = 0;
	}, 8000);
	
    sendResponse({farewell: "goodbye"});
  });