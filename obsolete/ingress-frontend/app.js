var options = {
  // time in ms when the event loop is considered blocked
  blockThreshold: 10
};

require('nodefly').profile(
    'eafb889dd36b2e6d8507969ba642fa7d',
    "ingressFrontend",
    options // optional
);

/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , api = require('./routes/api');
  
var app = module.exports = express();

var allowCrossDomain = function(req, res, next) {
    //console.log(req);
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, accept, origin, x-requested-with');

    next();
}

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.logger('dev'));
  app.use(express.favicon());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(allowCrossDomain);
  app.use(require('stylus').middleware({debug:true,src:__dirname + '/stylus',dest:__dirname + '/public'}));
  app.use(express.static(__dirname + '/public'));
  app.use(app.router);
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/heatmap', routes.heatmap);
app.get('/', routes.index);
app.get('/player', routes.player);
app.get('/playerlist', routes.playerlist);
app.get('/portal', routes.portal);
app.get('/portallist', routes.portallist);
app.get('/summary', routes.summary);

// JSON API

//app.get('/api/map/portals', api.portals);//returns portal counts per team
//app.get('/api/map/teamBalance', api.teamBalance);//returns teams and counts
app.get('/api/map/players', api.players);//returns name, level, id
//app.get('/api/map/player/:id', api.player);//returns name, level, id
//app.get('/api/map/portal/:id', api.portal);//returns name, level, id
app.get('/api/map/portals', api.portals);

//app.get('/api/playerdata/:date', api.playerdata);//returns general stats (players/action) for date
//app.get('/api/plext/top/:date/:action/:type', api.playerStat);//returns top 10 count of stat per date
//app.get('/api/plext/player/:id', api.playerPlexts);//returns all associated plexts
//app.get('/api/plext/portal/:id', api.portalPlexts);//returns all associated plexts
//app.get('/api/plext/portal/:id', api.playerStat);//returns all associated plexts
//app.get('/api/plext/dates', api.dates);//returns dates with data available
//app.get('/api/plext/dateblock/:date', api.dateBlock);//returns dates with data available


//app.get('/api/heatmap', api.heatmap);
//app.get('/api/heatmap/player/:id', api.heatmapPlayer);

// AUTOMATED SCANNER API

app.get('/api/nextportal/:node', api.nextportal);//next portal to scan
app.post('/api/postdata/:node', api.postdata);//api to post data

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode 4", this.address().port, app.settings.env);
});
