exports.heatmap = function(req, res){
  res.render('heatmap');
};
exports.index = function(req, res){
  res.render('index');
};
exports.player = function(req, res){
  res.render('player');
};
exports.playerlist = function(req, res){
  res.render('playerlist');
};
exports.portal = function(req, res){
  res.render('portal');
};
exports.portallist = function(req, res){
  res.render('portallist');
};
exports.summary = function(req, res){
  res.render('summary');
};